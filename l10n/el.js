OC.L10N.register(
    "user_oidc",
    {
    "Login with %1s" : "Σύνδεση με %1s",
    "ID4Me is disabled" : "Το ID4Me είναι απενεργοποιημένο",
    "Invalid OpenID domain" : "Μη έγκυρος τομέας OpenID",
    "Invalid authority issuer" : "Μη έγκυρος εκδότης αρχής",
    "Multiple authority found" : "Βρέθηκαν πολλαπλές αρχές",
    "The received state does not match the expected value." : "Η ληφθείσα κατάσταση δεν ταιριάζει με την αναμενόμενη τιμή.",
    "Authority not found" : "Δεν βρέθηκε αρχή",
    "Failed to decrypt the ID4ME provider client secret" : "Αποτυχία αποκρυπτογράφησης του μυστικού πελάτη του πάροχου ID4ME",
    "The received token is expired." : "Το ληφθέν κουπόνι έχει λήξει.",
    "The audience does not match ours" : "Το κοινό δεν ταιριάζει με το δικό μας",
    "The authorized party does not match ours" : "Η εξουσιοδοτημένη πλευρά δεν ταιριάζει με τη δική μας",
    "No authorized party" : "Καμία εξουσιοδοτημένη πλευρά",
    "The nonce does not match" : "Το nonce δεν ταιριάζει",
    "You must access Nextcloud with HTTPS to use OpenID Connect." : "Πρέπει να αποκτήσετε πρόσβαση στο Nextcloud με HTTPS για να χρησιμοποιήσετε το OpenID Connect.",
    "There is not such OpenID Connect provider." : "Δεν υπάρχει τέτοιος πάροχος OpenID Connect.",
    "Could not reach the OpenID Connect provider." : "Δεν ήταν δυνατή η επικοινωνία με τον πάροχο OpenID Connect.",
    "Failed to decrypt the OIDC provider client secret" : "Αποτυχία αποκρυπτογράφησης του μυστικού πελάτη του πάροχου OIDC",
    "Failed to contact the OIDC provider token endpoint" : "Αποτυχία επικοινωνίας με το endpoint token του πάροχου OIDC",
    "The issuer does not match the one from the discovery endpoint" : "Ο εκδότης δεν ταιριάζει με αυτόν από το endpoint ανακάλυψης",
    "Failed to provision the user" : "Αποτυχία παροχής του χρήστη",
    "You do not have permission to log in to this instance. If you think this is an error, please contact an administrator." : "Δεν έχετε δικαίωμα σύνδεσης σε αυτή την εμφάνιση. Αν πιστεύετε ότι πρόκειται για λάθος, παρακαλώ επικοινωνήστε με έναν διαχειριστή.",
    "User conflict" : "Σύγκρουση χρήστη",
    "OpenID Connect" : "OpenID Connect",
    "OpenID Connect user backend" : "Backend χρήστη OpenID Connect",
    "Use an OpenID Connect backend to login to your Nextcloud" : "Χρησιμοποιήστε ένα backend OpenID Connect για να συνδεθείτε στο Nextcloud σας",
    "Allows flexible configuration of an OIDC server as Nextcloud login user backend." : "Επιτρέπει την ευέλικτη διαμόρφωση ενός διακομιστή OIDC ως backend χρήστη σύνδεσης στο Nextcloud.",
    "Could not save ID4me state: {msg}" : "Δεν ήταν δυνατή η αποθήκευση της κατάστασης ID4me: {msg}",
    "Could not update the provider:" : "Δεν ήταν δυνατή η ενημέρωση του παρόχου:",
    "Could not remove provider: {msg}" : "Δεν ήταν δυνατή η αφαίρεση του παρόχου: {msg}",
    "Could not register provider:" : "Δεν ήταν δυνατή η εγγραφή του παρόχου:",
    "Allows users to authenticate via OpenID Connect providers." : "Επιτρέπει στους χρήστες να πιστοποιούνται μέσω παρόχων OpenID Connect.",
    "Enable ID4me" : "Ενεργοποίηση ID4me",
    "Registered Providers" : "Εγγεγραμμένοι Πάροχοι",
    "Register new provider" : "Εγγραφή νέου παρόχου",
    "Register a new provider" : "Εγγραφή νέου παρόχου",
    "Configure your provider to redirect back to {url}" : "Διαμορφώστε τον πάροχό σας να ανακατευθύνει πίσω στο {url}",
    "No providers registered." : "Δεν υπάρχουν εγγεγραμμένοι πάροχοι.",
    "Client ID" : "Αναγνωριστικό πελάτη",
    "Discovery endpoint" : "Endpoint ανακάλυψης",
    "Backchannel Logout URL" : "URL αποσύνδεσης Backchannel",
    "Redirect URI (to be authorized in the provider client configuration)" : "URI ανακατεύθυνσης (πρέπει να εγκριθεί στη διαμόρφωση του πελάτη του παρόχου)",
    "Update" : "Ενημέρωση",
    "Remove" : "Αφαίρεση",
    "Update provider settings" : "Ενημέρωση ρυθμίσεων παρόχου",
    "Update provider" : "Ενημέρωση παρόχου",
    "Submit" : "Υποβολή",
    "Client configuration" : "Διαμόρφωση πελάτη",
    "Identifier (max 128 characters)" : "Αναγνωριστικό (μέγιστο 128 χαρακτήρες)",
    "Display name to identify the provider" : "Εμφανιζόμενο όνομα για την αναγνώριση του παρόχου",
    "Client secret" : "Μυστικό πελάτη",
    "Leave empty to keep existing" : "Αφήστε κενό για να διατηρήσετε το υπάρχον",
    "Warning, if the protocol of the URLs in the discovery content is HTTP, the ID token will be delivered through an insecure connection." : "Προειδοποίηση, αν το πρωτόκολλο των URLs στο περιεχόμενο ανακάλυψης είναι HTTP, το ID token θα παραδοθεί μέσω μιας μη ασφαλούς σύνδεσης.",
    "Custom end session endpoint" : "Προσαρμοσμένο endpoint λήξης συνεδρίας",
    "Scope" : "Εύρος",
    "Extra claims" : "Επιπλέον αξιώσεις",
    "Attribute mapping" : "Αντιστοίχιση χαρακτηριστικών",
    "User ID mapping" : "Αντιστοίχιση ταυτότητας χρήστη",
    "Quota mapping" : "Αντιστοίχιση ποσόστωσης",
    "Groups mapping" : "Αντιστοίχιση ομάδων",
    "Extra attributes mapping" : "Αντιστοίχιση επιπλέον χαρακτηριστικών",
    "Display name mapping" : "Αντιστοίχιση εμφανιζόμενου ονόματος",
    "Gender mapping" : "Αντιστοίχιση φύλου",
    "Email mapping" : "Αντιστοίχιση email",
    "Phone mapping" : "Αντιστοίχιση τηλεφώνου",
    "Language mapping" : "Αντιστοίχιση γλώσσας",
    "Role/Title mapping" : "Αντιστοίχιση ρόλου/τίτλου",
    "Street mapping" : "Αντιστοίχιση οδού",
    "Postal code mapping" : "Αντιστοίχιση ταχυδρομικού κώδικα",
    "Locality mapping" : "Αντιστοίχιση τοπικής θέσης",
    "Region mapping" : "Αντιστοίχιση περιοχής",
    "Country mapping" : "Αντιστοίχιση χώρας",
    "Organisation mapping" : "Αντιστοίχιση οργανισμού",
    "Website mapping" : "Αντιστοίχιση ιστοσελίδας",
    "Avatar mapping" : "Αντιστοίχιση avatar",
    "Biography mapping" : "Αντιστοίχιση βιογραφίας",
    "Twitter mapping" : "Αντιστοίχιση Twitter",
    "Fediverse/Nickname mapping" : "Αντιστοίχιση Fediverse/Ψευδώνυμο",
    "Headline mapping" : "Αντιστοίχιση επικεφαλίδας",
    "Authentication and Access Control Settings" : "Ρυθμίσεις Πιστοποίησης και Ελέγχου Πρόσβασης",
    "Use unique user id" : "Χρήση μοναδικής ταυτότητας χρήστη",
    "By default every user will get a unique userid that is a hashed value of the provider and user id. This can be turned off but uniqueness of users accross multiple user backends and providers is no longer preserved then." : "Από προεπιλογή, κάθε χρήστης θα λάβει μια μοναδική ταυτότητα χρήστη που είναι μια τιμή κατακερματισμού του παρόχου και της ταυτότητας χρήστη. Αυτό μπορεί να απενεργοποιηθεί, αλλά η μοναδικότητα των χρηστών σε πολλαπλά backends χρηστών και πάροχους δεν διατηρείται πλέον.",
    "Use provider identifier as prefix for ids" : "Χρήση του αναγνωριστικού παρόχου ως πρόθεμα για τις ταυτότητες",
    "To keep ids in plain text, but also preserve uniqueness of them across multiple providers, a prefix with the providers name is added." : "Για να διατηρηθούν οι ταυτότητες σε απλό κείμενο, αλλά και να διατηρηθεί η μοναδικότητά τους σε πολλαπλούς πάροχους, προστίθεται ένα πρόθεμα με το όνομα του παρόχου.",
    "Use group provisioning." : "Χρήση παροχής ομάδων.",
    "This will create and update the users groups depending on the groups claim in the id token. The Format of the groups claim value should be {sample1} or {sample2} or {sample3}" : "Αυτό θα δημιουργήσει και θα ενημερώσει τις ομάδες των χρηστών ανάλογα με την αξίωση ομάδων στο id token. Η μορφή της τιμής της αξίωσης ομάδων πρέπει να είναι {sample1} ή {sample2} ή {sample3}",
    "Group whitelist regex" : "Regex λευκής λίστας ομάδων",
    "Only groups matching the whitelist regex will be created, updated and deleted by the group claim. For example: {regex} allows all groups which ID starts with {substr}" : "Μόνο οι ομάδες που ταιριάζουν με το regex της λευκής λίστας θα δημιουργηθούν, ενημερωθούν και διαγραφούν από την αξίωση ομάδων. Για παράδειγμα: {regex} επιτρέπει όλες τις ομάδες των οποίων το ID ξεκινά με {substr}",
    "Restrict login for users that are not in any whitelisted group" : "Περιορισμός σύνδεσης για χρήστες που δεν ανήκουν σε καμία ομάδα λευκής λίστας",
    "Users that are not part of any whitelisted group are not created and can not login" : "Οι χρήστες που δεν ανήκουν σε καμία ομάδα λευκής λίστας δεν δημιουργούνται και δεν μπορούν να συνδεθούν",
    "Check Bearer token on API and WebDav requests" : "Έλεγχος Bearer token σε αιτήματα API και WebDav",
    "Do you want to allow API calls and WebDav request that are authenticated with an OIDC ID token or access token?" : "Θέλετε να επιτρέψετε κλήσεις API και αιτήματα WebDav που πιστοποιούνται με ένα OIDC ID token ή access token?",
    "Auto provision user when accessing API and WebDav with Bearer token" : "Αυτόματη παροχή χρήστη κατά την πρόσβαση σε API και WebDav με Bearer token",
    "This automatically provisions the user, when sending API and WebDav Requests with a Bearer token. Auto provisioning and Bearer token check have to be activated for this to work." : "Αυτό παρέχει αυτόματα τον χρήστη, όταν αποστέλλονται αιτήματα API και WebDav με ένα Bearer token. Η αυτόματη παροχή και ο έλεγχος Bearer token πρέπει να είναι ενεργοποιημένοι για να λειτουργήσει αυτό.",
    "Send ID token hint on logout" : "Αποστολή ID token hint κατά την αποσύνδεση",
    "Should the ID token be included as the id_token_hint GET parameter in the OpenID logout URL? Users are redirected to this URL after logging out of Nextcloud. Enabling this setting exposes the OIDC ID token to the user agent, which may not be necessary depending on the OIDC provider." : "Θα πρέπει το ID token να συμπεριληφθεί ως παράμετρος GET id_token_hint στο URL αποσύνδεσης OpenID; Οι χρήστες ανακατευθύνονται σε αυτό το URL μετά την αποσύνδεση από το Nextcloud. Η ενεργοποίηση αυτής της ρύθμισης εκθέτει το OIDC ID token στον user agent, κάτι που μπορεί να μην είναι απαραίτητο ανάλογα με τον πάροχο OIDC.",
    "Cancel" : "Ακύρωση",
    "Domain" : "Τομέας",
    "your.domain" : "your.domain"
},
"nplurals=2; plural=(n != 1);");
