OC.L10N.register(
    "user_oidc",
    {
    "Client ID" : "Identif. klienta",
    "Update" : "Aktualizovat",
    "Remove" : "Odebrat",
    "Submit" : "Odeslat",
    "Client secret" : "Tajemství klienta",
    "Scope" : "Rozsah",
    "Attribute mapping" : "Mapování atributů",
    "Cancel" : "Storno",
    "Domain" : "Doména"
},
"nplurals=4; plural=(n == 1 && n % 1 == 0) ? 0 : (n >= 2 && n <= 4 && n % 1 == 0) ? 1: (n % 1 != 0 ) ? 2 : 3;");
